#!/usr/bin/env python
#
# A RDFlib- and pywikipediabot-based SMW data population
# based on OWl/RDF parsing
# 

from string import Template
import isodate            # rdflib dependencies
import rdflib
import wikipedia          # pywikipediabot

class Rdf_to_smw:
    """
    A method to parse a RDF/XML file and get the triples to be uploaded via
    pywikipediabot to a SMW
    """
    
    def __init__(self):
        self.data = {}
    
    def parse_rdf(self, rdf):
        """
        Parses a big rdf to a sleepycat local store
        @param rdf the xml/rdf file to be parsed
        @return graph the parsed graph
        """
        graph = rdflib.Graph('Sleepycat')
        graph.open('store', create = True)
        graph.parse(rdf)
        return(graph)
    
    def parse_subject(self, subject):
        """ Strips the URL from a given subject """
        s = subject.encode()
        return (s.split('/')[-1])
    
    def parse_predicate(self, predicate):
        """ Strips the URL from a given predicate """
        p = predicate.encode()
        return (p.split('#')[-1])
    
    def parse_object(self, value):
        """ Strips the URL from a given object """
        v = value.encode()
        return(v.split('/')[-1])
                
    def indiv_generator(self, graph):
        """
        Gets all the predicates and objects (= values) matching a given subject and
        returns it as a nested dict
        
        @return 
        """        
        resultd = {}
        
        prev = ''
        for subject, predicate, value in graph:
            s = self.parse_subject(subject)
            if not s in resultd.keys(): resultd[s] = {}
            else: resultd[s][self.parse_predicate(predicate)] = self.parse_object(value) 
            if subject != prev and prev != '':
                yield resultd
            prev = subject
    
    # #nonfunctional
    # def plain_test(self, graph):
    #     with open('/tmp/test', 'w') as graph:             
    #         i = 0
    #         for subject, predicate, value in graph:
    #             s = self.parse_subject(subject)
    #             p = self.parse_predicate(predicate)
    #             v = self.parse_value(value)
    #             fh.write(s + p + v + '\n')
    #             i += 1
    #             if i > 100: break

# tests

# page put start

# class Wiki_editor:
#     def __init__(self, pagename, content):
#         self.pagename = pagename
#         self.content = content
         
#     def main():
#         pagename = 'A1'
#         site = wikipedia.getSite()
#         page = wikipedia.Page(site, pagename)
#         wikipedia.output(u"Loading %s..." % pagename)
#         try:
#             text = page.get()
#         except wikipedia.NoPage: 
#             text = ''
#         except wikipedia.IsRedirectPage: 
#             wikipedia.output(u'%s is a redirect!' % pagename)
#             exit()
#         except wikipedia.Error: 
#             wikipedia.output(u'Some Error, skipping..')
#             exit()
#         newtext = text + '\nTesting'
#         page.put(newtext, comment='Bot: Appending content', watchArticle = None, minorEdit = True)  
#     def process(self):
#         site = wikipedia.getSite()
#         page = wikipedia.Page(site, self.pagename)
#         page.put(self.content, comment='Bot: Appending content')
         
#     if __name__ == '__main__':
#         try:
#             #main()
#             process(self.pagename, self.content)
#         finally:
#             wikipedia.stopme()


def process(pagename, content):
    site = wikipedia.getSite()
    page = wikipedia.Page(site, pagename)
    try:
        text = page.get()
    except wikipedia.NoPage: 
        text = ''
    except wikipedia.IsRedirectPage: 
        wikipedia.output(u'%s is a redirect!' % pagename)
        exit()
    except wikipedia.Error: 
        wikipedia.output(u'Some Error, skipping..')
    page.put(text + content, comment='Bot: Appending content')


def process_only_new(pagename, content):
    site = wikipedia.getSite()
    page = wikipedia.Page(site, pagename)
    try:
        text = page.get()
    except wikipedia.NoPage: 
        page.put(content, comment='Bot: Create page')
    except wikipedia.IsRedirectPage: 
        wikipedia.output(u'%s is a redirect!' % pagename)
        exit()
    except wikipedia.Error: 
        wikipedia.output(u'Some Error, skipping..')


def clean_subject(s):
    if 'GO' in s:
        subject = 'GO:' + s.split('_')[-1]
    elif 'SO' in s:
        subject = 'SO:' + s.split('_')[-1]
    else:
        subject = s.split('/')[-1].split('#')[-1]
    
    return subject

# page put end

foo = Rdf_to_smw()
# editor = Wiki_editor()

graph = foo.parse_rdf('/home/labs/maplab/imallona/tmp/merged.owl')


prev_s = ''
curr_list = []
for s, p, v in graph:
    if s != prev_s:
        content = ''
        for pair in curr_list:
            prop = pair[0]
            value = pair[1]
            if prop == 'has_mc' or prop == 'has_h' or prop == 'has_beta_value':
                content = """
%s
{{#arraydefine:%s|%s|,|}}

{{#arrayprint:%s |<br/> |@@@@ |[[%s::@@@@]]}}
{{meth_status|1 ={{#arrayprint:%s}}}}
"""  %(content, prop, value, prop, prop, prop)
            else:
                content = '%s\n[[%s::%s]]' %(content, prop, value)
        
        # process(pagename = clean_subject(s),
                # content = content)
        
        process_only_new(pagename = clean_subject(s),
                         content = content)
        
        # cleaning, so new properties and values will be added    
        curr_list = []
    
    cprop = p.split('#')[-1]
    cvalue = v.split('#')[-1]
    
    curr_list.append([cprop, cvalue])
        
    prev_s = s

# i = 0
# for s, p, v in graph:
#     i += 1
    
#     prop = p.split('#')[-1]
#     value = v.split('#')[-1]
    
#     if prop == 'has_mc' or prop == 'has_h' or prop == 'has_beta_value':
#         process(pagename = s.split('#')[-1],
#                 content = """
# {{#arraydefine:%s|%s|,|print=list}}
# {{#arrayprint:%s |<br/> |@@@@ |[[%s::@@@@]] }}

# {{#arrayprint:b||@@@@|<nowiki/>
# * has %s of @@@@
# }}
# """ %(prop,value, prop, prop, prop))
    
#     else:
#         process(pagename = s.split('#')[-1], 
#                 content = '[[%s::%s]]' %(prop, value))

