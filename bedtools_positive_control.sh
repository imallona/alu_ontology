#!/bin/bash
#
# 7 May 2015

set -e

NUM_THREADS=1
BASE=$HOME/pfc/bedtools_shuffled
NPERM=1000
LISTER_SCRIPT="$SRC"/ontology/methylation_utils.sh
#the previous produces its outputs at
LISTER_BEDS=~/tmp/lister/beds/

HMMS=(10_Txn_Elongation 11_Weak_Txn 12_Repressed 13_Heterochrom/lo 14_Repetitive/CNV 15_Repetitive/CNV 1_Active_Promoter 2_Weak_Promoter 3_Poised_Promoter 4_Strong_Enhancer 5_Strong_Enhancer 6_Weak_Enhancer 7_Weak_Enhancer 8_Insulator 9_Txn_Transition)

PROMOTERS=(1_Active_Promoter 2_Weak_Promoter 3_Poised_Promoter)

mkdir -p "$BASE"

cd "$BASE"

echo 'Retrieving the Alu coordinates'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT genoName, genoStart,genoEnd,repName,repClass,strand,repFamily \
     FROM hg19.rmsk \
     WHERE repFamily = "Alu" \
      AND repName LIKE "Alu%"' | awk '{if (NR!=1) {print}}' > "$BASE"/alus.bed


echo 'Retrieving chromatin colors'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT chrom, chromStart, chromEnd, name 
    FROM hg19.wgEncodeBroadHmmH1hescHMM' | awk '{if (NR!=1) {print}}' > "$BASE"/colors.bed

echo 'Retrieving methylation data from Lister'

bash "$LISTER_SCRIPT" lister

echo 'Retrieving genes'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT chrom, min(txStart), max(txEnd), strand, name2
    FROM hg19.ensGene  
    GROUP BY name2' |  awk '{if (NR!=1) {print}}' > "$BASE"/genes.bed

echo Sorting

sort -k1,1 -k2,2n alus.bed > foo; mv foo alus.bed
sort -k1,1 -k2,2n colors.bed > foo; mv foo colors.bed
sort -k1,1 -k2,2n genes.bed > foo; mv foo genes.bed

echo 'Sorting according to genes; genes are the ones with up- or downstream features, not the alus'

mkdir -p $BASE/locations/color

bedtools closest -a alus.bed -b genes.bed -D "b"  > alus_genes.intersect

## downstream means the alu is downstream of the gene
awk '$13 > 0 && $13 < 1001' alus_genes.intersect > $BASE/locations/alus_genes_downstream.bed
awk '$13 < 0 && $13 > -1001' alus_genes.intersect > $BASE/locations/alus_genes_upstream.bed
awk '$13 == 0' alus_genes.intersect > $BASE/locations/alus_genes_inside.bed

echo 'Coloring'

for fn in $(ls $BASE/locations/*bed) 
do
    fn=$(basename "$fn")
    bedtools intersect -a $BASE/locations/$fn  \
        -b $BASE/colors.bed -wo -sorted > $BASE/locations/color/"$fn".color

    for promoter in ${PROMOTERS[@]} 
    do
        fgrep $promoter $BASE/locations/color/"$fn".color > $BASE/locations/color/"$fn".$promoter
    done
    
done

echo 'Getting the methylation'

mkdir -p $BASE/locations/meth

for meth_chrom in $(ls "$LISTER_BEDS"/mc*bed)
do
    for promoter in ${PROMOTERS[@]} 
    do
        # this contains the colors
        for fn in $(ls $BASE/locations/color/*"$promoter")
        do
             fn=$(basename "$fn")
             meth_chrom=$(basename "$meth_chrom")
             bedtools intersect -a  $BASE/locations/color/$fn \
                 -b $LISTER_BEDS/$meth_chrom -wa -wb  > $BASE/locations/meth/"$meth_chrom"_"$fn"

        done

    done
done


echo 'Getting the betas dividing mc and h'

mkdir -p $BASE/locations/betas

for fn in $(ls $BASE/locations/meth/mc*)
do
    awk '{OFS=FS="\t"; print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$25/$26}' $fn > \
        $BASE/locations/betas/$(basename $fn)
done


echo 'Getting either mean, either sd for each Alu'


summaries=(mean stdev)

for summary in ${summaries[@]}
do
    mkdir -p $BASE/locations/$summary
    
    for fn in $(ls $BASE/locations/betas)
    do
        bedtools groupby -i $BASE/locations/betas/$fn -g 1,2,3,4,5,6,7,8,9,10,11,12,13 -c 14 -o $summary \
            > $BASE/locations/$summary/$(basename $fn)
    done
done


echo 'R plotting with bedtools_positive_control.R'



echo 'Shuffled background to test for enrichments'
echo 'Being testing right now!'

IN=$BASE/shuffled/in
MID=$BASE/shuffled/mid
OUT=$BASE/shuffled/out

mkdir -p $IN $MID $OUT

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e  "SELECT chrom, size FROM hg19.chromInfo" \
    > $BASE/hg19.genome


for i in $(seq 1 $NPERM)
do
    bedtools shuffle -i $BASE/alus.bed -g $BASE/hg19.genome -noOverlapping -seed $i | sortBed > \
        $IN/shuffled_"$i".bed
    bedtools intersect -a $BASE/shuffled/in/shuffled_"$i".bed  \
        -b $BASE/colors.bed -wo -sorted > $MID/shuffled_"$i".color

    # coloring
    for promoter in ${PROMOTERS[@]} 
    do
        fgrep $promoter $MID/shuffled_"$i".color > $MID/shuffled_"$i".$promoter
    done

    # assigning the DNA meth


    for meth_chrom in $(ls "$LISTER_BEDS"/mc*bed)
    do
        for promoter in ${PROMOTERS[@]} 
        do
            # this contains the colors
            for fn in $(ls $MID/shuffled*"$promoter")
            do
                fn=$(basename "$fn")
                meth_chrom=$(basename "$meth_chrom")
                bedtools intersect -a  $MID/$fn \
                    -b $LISTER_BEDS/$meth_chrom -wa -wb  > $MID/"$meth_chrom"_"$fn"

            done
        done
    done

   ## betas

    mkdir -p $MID/betas

    for fn in $(ls $MID/mc*shuffled_"$i"*Promoter)
    do
        awk '{OFS=FS="\t"; print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$19/$20}' $fn > \
            $MID/betas/$(basename $fn)
    done


    # getting either mean, either sd for each Alu

    summaries=(mean stdev)

    for summary in ${summaries[@]}
    do
        mkdir -p $OUT/$summary
        
        for fn in $(ls $MID/betas/mc*shuffled_"$i"*Promoter)
        do
            bedtools groupby -i $fn -g 1,2,3,4,5,6,7,8,9,10,11,12,13 -c 14 -o $summary \
                > $OUT/$summary/$(basename $fn)
        done
    done




    # Cleaning (partly)
    rm $IN/shuffled_"$i".bed $MID/shuffled_"$i"* $MID/mc*shuffled_"$i"*Promoter
done
