#!/usr/bin/env python

"""
@package alu_ontology
@author Izaskun Mallona
Fetches the methylation from Lister's data to include it into the Alu ontology
"""

from os import path as op
from unix_subprocesses import *
import glob


class Methylation:

    # the bash script that fetches and parses the Lister's data
    METHYLATION_UTILS = op.join(op.dirname(op.realpath(__file__)), 'methylation_utils.sh')
    
    TMP= '/home/labs/maplab/imallona/tmp'
    # the results folder of summarized methylation values
    GROUPED_BY_FOLDER = op.join(TMP, 'lister', 'grouped_by')

    # documented at http://neomorph.salk.edu/human_methylome/data.html
    LISTER_FN = 'mc_h1.tar.gz'
    LISTER_FTP = 'ftp://ftpuser3:s3qu3nc3@neomorph.salk.edu/mc'
    
    # contains the hESC data from both two replicates
    LISTER_DATA = op.join(LISTER_FTP, LISTER_FN)
    
    """
    Deals with Lister's methylation data
    """
    def __init__(self):
        self.unix = Unix_subprocesses()

    def get_methylation_tgzfile(tgzfile_url, tgzfile_fn, tmp):
        """
        @deprecated, now done by bash scripting
        Gets the Lister's FTP data and uncompresses it
        @param tgzfile_url the tgz file url
        @param tmp the temp folder the download will be done at
        @return the name of the folder containing the extracted data
        """
        self.unix.wget_getter(tgzfile_url, op.join(tmp, tgz_fn))
        self.unix.tgz_extract(op.join(tmp, tgz_fn))
        return (op.basename(tgzfile_fn).split('.')[0])
    
    def fetch_lister_data(self):
        """
        Processes the  Lister's data using a subprocess
        """
        (out, err) = self.unix.call_bash_script(self.METHYLATION_UTILS + ' ' +  'lister')

        return (out, err)
        
    def generate_beta_values(self, mc, h):
        """
        @param mc a blob of methylated cytosines like '1,2,3,4'
        @param h a blob of total reads like '1,3,3,5'
        @return a blob of mc/h
        """
        
        return(','.join(map(str,[float(mc)/float(h) 
                                 for mc, h  
                                 in zip(mc.split(','), h.split(',' ))])))
    
        
    def get_meth_values(self, rd, mc):
        """ 
        Generates a dict containing the mc and h blobs for each alu; its key
        is the Alu chrom, start and end concatenated using '_'
        @param rd the result dict
        @param mc an mc archive produced by fetch_lister_data()
        """        
        # rd = {}
        mch = open(mc, 'r')
        h = mc.replace('_mc.bed', '_h.bed')
        hh = open(h, 'r')
        
        for line in mch:
            item = line.split('\t')
            alu_id = '_'.join(item[0:3])
            curr_mc = item[3]
            pair = hh.readline().split('\t')
            h_alu_id = '_'.join(pair[0:3])
            curr_h = pair[3]
            if alu_id == h_alu_id:
                rd[alu_id] = (curr_mc.strip(), 
                              curr_h.strip(), 
                              self.generate_beta_values(curr_mc, curr_h))
            else: print 'Something went wrong here'
        
        mch.close()
        hh.close()
        
        return(rd)

    def process_methylation(self):
        """
        Main method for Lister's methylation analysis"
        """
        #@todo change dthis
        print ('If stuck, the bash script is waiting for a "y" by stdin')
        (out, err) = self.fetch_lister_data()
        print (out)
        
        mc_files = glob.glob(op.join(self.GROUPED_BY_FOLDER, '*mc.bed'))
        
        # the dict is updated for each chromosome sequentially
        meth_values = {}
        i = 0
        for i in range(len(mc_files)):
            meth_values = self.get_meth_values(meth_values, mc_files[i])
        
        return(meth_values)
