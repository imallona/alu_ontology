#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@package alu_ontology
@author Izaskun Mallona
Gets the XML/RDF formalization of an OWL ontology of human Alus
"""

from string import Template

class Formatters:    
    """
    The template-based RDF/XML formatting tool for Protégé and Pellet-compliant
    ontologies

    """
    def format_gene_ontology(self, go_id, go_descr):
        """
        The Gene Ontology item formatter.
        
        @todo Mind that the go must be slimmed to the go slim prsent at the ontology level
        using map2slim.pl
        available to at http://amigo.geneontology.org/cgi-bin/amigo/slimmer
        @param go_id string, the gene ontology id
        @param go_descr string, the gene ontology description
        @return the substituted template
        """

        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#$go_descr -->

        <owl:Thing rdf:about="&alu_ontology;$go_descr">
            <rdf:type rdf:resource="&obo;$go_id"/>
            <rdf:type rdf:resource="&owl;NamedIndividual"/>
        </owl:Thing>
        """)

        return t.substitute(go_id = go_id, go_descr = go_descr)

    def format_gene(self, ensg, loc):
        """
        The gene formatter.
        
        Mind that this gets the cdsStart and cdsEnd of the ensGene table and thus the information
        passed to the different transcript variants does not contain which exons are mantained;
        a reimplementation of the transcript individuals including txStart and txEnd may make
        sense @todo
        @param ensg string, the ensemble gene id
        @param loc string, the location
        @return the substituted template
        """

        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#$ensg -->

        <owl:Thing rdf:about="&alu_ontology;$ensg">
            <rdf:type rdf:resource="&alu_ontology;gene"/>
            <rdf:type rdf:resource="&owl;NamedIndividual"/>
            <ro:located_in rdf:resource="&alu_ontology;$loc"/>
        </owl:Thing>
        """)

        return t.substitute(ensg = ensg, loc = loc)

    def format_location(self, chrom, start, end, strand):
        """
        Formats a genomic coordinate that will be associated to a given feature, such as 
        a transcript, gene, mark or the like
        @param strand must be either 'positive' or 'negative'
        @return a tuple containing the loc accession as well the owl substituted template
        """

        # if strand == '+': strand = 'positive'
        # elif strand == '-': strand = 'negative'
        if not str(chrom).startswith('chr'): chrom = 'chr' + str(chrom)

        # the accession or identifier that will be used to assign the location to a feature
        acc_t = Template('Loc.$chrom:$start-$end;$strand')

        # the owl code to represent the individual
        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#Loc.$chrom:$start-$end;$strand -->

        <owl:Thing rdf:about="#Loc.$chrom:$start-$end;$strand">
            <rdf:type rdf:resource="#biological_region"/>
            <on_chromosome rdf:datatype="&xsd;string">$chrom</on_chromosome>
            <has_start_point rdf:datatype="&xsd;integer">$start</has_start_point>
            <has_end_point rdf:datatype="&xsd;integer">$end</has_end_point>
            <on_strand rdf:datatype="&xsd;string">$strand</on_strand>
        </owl:Thing>
        """)

        return (t.safe_substitute(chrom = chrom, start = start, end = end, strand = strand),
                acc_t.safe_substitute(chrom = chrom, start = start, end = end, strand = strand))


    def format_transcript(self, enst, ensg, bp, cc, mf):
        """
        Formats a transcript and its annotations (GO)
        Takes as many bp, cc and mf annotations as provided (lists)
        @param enst string the enst
        @param ensg string the ensg
        @param bp list of strings of biological processes
        @param cc list of strings of cellular locations
        @param mf list of strings of molecular functions
        @return the substituted template
        """

        t = ''
        header_t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#$enst -->

        <owl:Thing rdf:about="&alu_ontology;$enst">
            <rdf:type rdf:resource="&obo;owl/SO#SO_0000833"/>
            <rdf:type rdf:resource="&owl;NamedIndividual"/>
            <obo2:transcribed_from rdf:resource="&alu_ontology;$ensg"/>""")

        footer = """
        </owl:Thing>
        """

        t += header_t.substitute(enst = enst, ensg = ensg)

        if len(bp) > 0:
            bp_t = Template("""
            <biological_process_player rdf:resource="&alu_ontology;$bp"/>""")
            for item in bp: t += bp_t.substitute(bp = item)

        if len(cc) > 0:
            cc_t = Template("""
            <located_in_cellular_component rdf:resource="&alu_ontology;$cc"/>""")        
            for item in cc: t += cc_t.substitute(cc = item)

        if len(mf) > 0:
            mf_t = Template("""
            <involved_in_molecular_function rdf:resource="&alu_ontology;$mf"/>""")
            for item in mf: t += mf_t.substitute(mf = item)

        t += footer

        return(t)
    
    def format_alu(self, chrom, start, end, strand, distance, ensg, 
                   family, length, color, tfbs, meth):
        """
        Formats a given Alu
        @param chrom string the chromosome
        @param start string the start
        @param end string the end
        @param strand string the strand
        @param distance int the distance to a given gene
        @param ensg string the ensembl gene id
        @param family string the alu family
        @param length int the alu length
        @param color string the HMM chromatin state color
        @param tfbs the <tag> delimited and formatted transcription factors binding sites
        @param meth the <tag> delimited and formatted meth values
        @return the substituted Alu template
        @todo change the length computation to be done with the location
        @todo change the way the tfbs are dealed with
        """
 
        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#alu.$chrom:$start-$end;$strand -->

        <owl:Thing rdf:about="&alu_ontology;alu.$chrom:$start-$end;$strand">
            <rdf:type rdf:resource="&alu_ontology;Alu"/>
            <rdf:type rdf:resource="&owl;NamedIndividual"/>
            <has_distance_to_nearest_gene rdf:datatype="&xsd;int">$distance</has_distance_to_nearest_gene>
            <has_length rdf:datatype="&xsd;integer">$length</has_length>
            <has_Alu_family rdf:datatype="&xsd;string">$family</has_Alu_family>
            <closest_gene_to_Alu rdf:resource="&alu_ontology;$ensg"/>
            <ro:located_in rdf:resource="&alu_ontology;Loc.$chrom:$start-$end;$strand"/>
            <closest_chromatin_state_to_Alu rdf:resource="&alu_ontology;$color"/>
            $tfbs
            $meth
        </owl:Thing>
        """)

        return t.substitute(chrom = chrom, start = start, end = end, 
                            strand = strand, distance = distance, ensg = ensg,
                            family = family, length = length, color = color, 
                            tfbs = tfbs, meth = meth)


    def format_color(self, chrom, start, end, name, strand, line):
        """
        Formats the hmm_hesc data retrieved with the hmm_hesc_color_getter() function
        Mind that the HMM do not have 'strand', although is requested
        @param chrom string the chromosome
        @param start string the start
        @param end string the end
        @param strand string the strand
        @param name string the HMM chromatin state color
        @param line string the cell line the HMM was defined at
        @return the substituted chromatin state template
        """

        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#hmm.$chrom:$start-$end -->

        <owl:Thing rdf:about="hmm.$chrom:$start-$end">
            <rdf:type rdf:resource="$name"/>
            <ro:located_in rdf:resource="&alu_ontology;Loc.$chrom:$start-$end"/>
            <has_cell_line rdf:resource="$line"/>
        </owl:Thing>
        """)

        return t.substitute(chrom = chrom, start = start, 
                            end = end, name = name, strand = strand, 
                            line = line)

    def format_tfbs(self, key, tfbs_dict):
        """
        @param tfbs_dict the dict of tfbs for a given Alu; the key is a 
           the Alu chrom, start, end joined by '_'
        @return string the <has_tfbs> flanked tfbs, not an individual
        """
        
        substituted = ''
        t = Template('<has_tfbs>$tfbs</has_tfbs>\n')
        
        tfbs = tfbs_dict[key].split(',')
        for item in tfbs: 
            if item != '.' : substituted += t.substitute(tfbs = item)
        
        return substituted
            

    def format_meth(self, key, meth_dict):
        """
        @param tfbs_meth the dict containing the meth data; the key contains
           the Alu chrom, start, end joined by '_'
        @return the substituted template with the mc and h number of reads plus
           the beta values
        """
        
        t = Template("""          
            <has_mc>$mc</has_mc>
            <has_h>$h</has_h>
            <has_beta_value>$beta</has_beta_value>
            """)
        
        #mc, h, beta = meth_dict[key].split(',')[0:3]
        mc = meth_dict[key][0]
        h = meth_dict[key][1]
        beta = meth_dict[key][2]
        
        return(t.substitute(mc = mc, h = h, beta = beta))
