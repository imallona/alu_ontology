#!/usr/bin/env python
#
# ontology_population.py
#
# Gets the XML/RDF formalization of an OWL ontology of human Alus
#
# Izaskun Mallona, 2013, GPL v2

"""
@package alu_ontology
@author Izaskun Mallona
Gets the XML/RDF formalization of an OWL ontology of human Alus
"""


import sys
import os
import shutil
import re
from parsers import *
from ucsc_mysql_shuttle import *
from unix_subprocesses import *
from utils import *
from transfac import * 
from methylation import *

TMP= '/home/labs/maplab/imallona/tmp/new_ontology'
# TMP= '/imppc/labs/maplab/imallona/tmp/new_ontology'
INDIVIDUALS_FN = op.join(TMP, 'individuals2.owl')
GO_TERMS_FN = op.join(TMP, 'go.owl')
DATA = '/imppc/labs/maplab/imallona/src/ontology/data'

class Main:    
    """
    Builds either a Protege-compliant OWL or a RDFIO-compliant one.
    """    
    def build_alu_ontology(rdfio_compliant, chrom):
        """
        Main method of ontology making
        @param rdfio_compliant boolean, whether the output is desired to be piped to RDFIO /SMW 
        or to Protege
        @param chrom the chromosome to process
        """
   
        shuttle = Ucsc_mysql_shuttle()
        db = shuttle.db_connect()

        data = shuttle.ucsc_data_getter(db, chrom)
        genes = shuttle.gene_location_getter(db)
        alus = shuttle.alu_getter(db, chrom)
        colors = shuttle.hmm_hesc_color_getter(db, chrom) 

        if op.exists(INDIVIDUALS_FN): os.remove(INDIVIDUALS_FN)
        if op.exists(GO_TERMS_FN): os.remove(GO_TERMS_FN)
        if not op.exists(TMP): os.makedirs(TMP)

        indiv_fh = open(INDIVIDUALS_FN, 'a')
        go_terms_fh = open(GO_TERMS_FN, 'a')

        # iteration based in retrieving all the annotations for a given transcript
        # 
        # the individuals owl file is generated
        # as well the go should; but some go slimming must be introduced here @todo
        parsers = Parsers(rdfio_compliant)
        parsers.parse_genes(indiv_fh, data, genes)
        
        unix = Unix_subprocesses()
        closest_gene = unix.assign_closest_gene(alus, genes, TMP)
        closest_color = unix.assign_closest_hmm(alus, colors, TMP)
        
        merged = unix.merge_genes_and_chromatin_states(closest_gene, closest_color, TMP)
        
        print '[%s] TFBS start' %(Utils().timestamp())

        transfac = Transfac()
        tfbs_fn = transfac.get_big_bed(TMP)
        alus_fn = op.join(TMP, 'alus.bed')
        with open(alus_fn, 'w') as alus_fh:
            for alu in alus:
                alus_fh.write('\t'.join(map(str, alu)) + '\n')
                
                
        tfbs_intersected = unix.summarize_intersecting_features(alus_fn, tfbs_fn)
        tfbs_dict = parsers.parse_tfbs(tfbs_intersected) 
        
        print '[%s] TFBS end' %(Utils().timestamp())

        print '[%s] Methylation start' %(Utils().timestamp())

        meth_dict = Methylation().process_methylation()
        print(len(meth_dict.keys()))
        
        print '[%s] Methylation end' %(Utils().timestamp())
        print '[%s] Data integration start' %(Utils().timestamp())

        parsers.parse_merged_colors_and_genes(indiv_fh, merged, tfbs_dict, meth_dict)
        
        print '[%s] Data integration end' %(Utils().timestamp())
        # print 'Data integration end\nColor parsing start'
        # parsers.parse_colors(indiv_fh, colors, 'hESC')
        
        # print 'Color parsing end'
        db.close()
        indiv_fh.close()
        go_terms_fh.close()
        
        print 'Output writing start'
        if rdfio_compliant: 
            unix.rdfio_structure_builder(DATA, INDIVIDUALS_FN, op.join(TMP, 'for_rdfio.owl' %chrom))
        else:
            unix.ontology_structure_builder(DATA, INDIVIDUALS_FN, op.join(TMP, 'merged.owl'))
            unix.ontology_structure_builder(DATA, INDIVIDUALS_FN, op.join(TMP, '%s_merged.owl' %str(chrom)))
        print 'done'

    for chrom in range(1,23) + ['X', 'Y']:
    # for chrom in range(22,23):        
        build_alu_ontology(rdfio_compliant = False, chrom = 'chr%s' %chrom)

