#!/usr/bin/env python

class Preformatted_query:
    UCSC_BULK_ALL = """
    SELECT  eg.name as enst, eg.name2 as ensg, eg.chrom, eg.strand, 
            eg.txStart, eg.txEnd, eg.cdsStart, eg.cdsEnd, 
            xref.geneSymbol, 
            go.goId, got.name, got.term_type 
    FROM    hg19.ensGene as eg, hg19.kgXref as xref, go.goaPart as go, 
            hg19.knownToEnsembl as kte, go.term as got 
    WHERE   kte.value = eg.name AND 
            kte.name = xref.kgID AND 
            xref.spId = go.dbObjectId AND 
            go.goID = got.acc;
    """

    UCSC_ALU_ALL = """
    SELECT genoName, genoStart,genoEnd,repName,repClass,strand,repFamily
    FROM hg19.rmsk 
    WHERE repFamily = "Alu" 
    AND repName LIKE "Alu%"
    """

    UCSC_GENE_LOCATIONS = """
    SELECT name2, chrom, min(txStart), max(txEnd), strand 
    FROM hg19.ensGene  
    GROUP BY name2
    """

    UCSC_BROAD_HMM_H1 = """
    SELECT chrom, chromStart, chromEnd, name 
    FROM hg19.wgEncodeBroadHmmH1hescHMM;
        """

    # UCSC_BULK_ALL variant
    def ucsc_bulk_chrom_sql(self, chrom):
        tiny_query = """
        SELECT  eg.name as enst, eg.name2 as ensg, eg.chrom, eg.strand, eg.txStart, 
                eg.txEnd, eg.cdsStart, eg.cdsEnd, 
                xref.geneSymbol, 
                go.goId, got.name, got.term_type 
        FROM    hg19.ensGene as eg, hg19.kgXref as xref, go.goaPart as go, 
                hg19.knownToEnsembl as kte, go.term as got 
        WHERE   kte.value = eg.name AND 
                kte.name = xref.kgID AND 
                xref.spId = go.dbObjectId AND 
                go.goID = got.acc AND
                eg.chrom = '%s';
        """ %chrom
        
        return tiny_query

    # UCSC_ALU_ALL variant
    def ucsc_alu_sql(self, chrom):
        tiny_query = """
        SELECT genoName, genoStart, genoEnd, repName, repClass, strand, repFamily
        FROM hg19.rmsk 
        WHERE repFamily = "Alu" 
        AND repName LIKE "Alu%" AND
        genoName = '""" + chrom + "';"
       
        return tiny_query

    # UCSC_BROAD_HMM_H1 variant
    def ucsc_hmm_h1_broad(self, chrom):
        tiny_query = """
        SELECT chrom, chromStart, chromEnd, name 
        FROM hg19.wgEncodeBroadHmmH1hescHMM
        WHERE chrom = '%s';""" %chrom
        
        return tiny_query
