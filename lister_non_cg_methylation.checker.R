#!/usr/bin/env Rscript
#
# lister_non_cg_methylation_checker.R
#
# Checks the concordance between a local methylation value (CHH, CGH) and the surrounding
# canonical CG values
# i.e.
# input: a sorted set of meth values of CG CG CG CHH CG CG CG
# output: a cor.test() and a plot of the mean methylation levels of CG against the CHH central item,
#   given a flank size of 3
#
# Izaskun Mallona, Jan 2014, GPLv2
#
# According to this result, it was decided to skip non CG data

FN = '/home/labs/maplab/imallona/tmp/lister/mc_h1/mc_h1_19'
FLANK = 1     # the number of CG sites flanking a non CG site (microenvironment)
MAX = 500      # the num of environments scrutinized

# 4 is the first quartile
MINH = 4       # min number of reads of a position to be taken into account 

# skipping the telomeric sequences; just starting after 10000 CpGs (assumes sorted input)
dat = read.table(FN, sep = "\t", header = F, skip = 100000)

# h is the total of reads, mc the total of methylated reads
colnames(dat) <- c('chr', 'start', 'strand', 'methType', 'mc', 'h')

# filtering
dat <- dat[dat$h >= MINH,]

i = 0         # the maximum number of non CGsites to be scrutinized
j = FLANK + 1 

chg = list( cg = vector(), chg = vector())
chh = list( cg = vector(), chh = vector() )
control = list ( cg = vector(), control = vector() )

while ( length(chg[[1]]) <= MAX || length(chh[[1]]) <= MAX || length(control[[1]]) <= MAX) {
  env <- dat[c((j-1):(j-FLANK), (j+1):(j+FLANK)),]

  if ( dat[j,]$methType != 'CG' ) {
    
    if( unique(env$methType) == 'CG' && dat[j,]$methType == 'CHG' ) {
      chg[['cg']] <- c(chg[['cg']], mean(env$mc/env$h))
      chg[['chg']] <- c(chg[['chg']], dat[j,]$mc/dat[j,]$h)
    }

    else if ( unique(env$methType) == 'CG' && dat[j,]$methType == 'CHH' ) {
      chh[['cg']] <- c(chh[['cg']], mean(env$mc/env$h))
      chh[['chh']] <- c(chh[['chh']], dat[j,]$mc/dat[j,]$h)
    }

  } else {
    # just controls
    
    if( unique(env$methType) == 'CG' && dat[j,]$methType == 'CG' ) {
      control[['cg']] <- c(control[['cg']], mean(env$mc/env$h))
      control[['control']] <- c(control[['control']], dat[j,]$mc/dat[j,]$h)
    }
        
  }
  
  j = j + FLANK*2 + 1
  if (j %% 1000 == 0) print('iterated over 1000')
}

pdf(paste('lister_cpg_environments_flank_', FLANK, '_num_environments_',  MAX,
          '_min_reads_', MINH, '.pdf', sep = ''),
    useDingbats = FALSE)
 
plot(chg[['cg']], chg[['chg']],
     main = sprintf('CHG\n%s flanking cpgs and %s environments scrutinized\n pvalue %s, cor %s',
       FLANK,
       MAX,
       cor.test(chg[['cg']], chg[['chg']])$p.value,
       cor.test(chg[['cg']], chg[['chg']])$estimate),
     xlim = c(0,1),
     ylim = c(0,1),
     xlab = 'surrounding cgs methylation (beta value)',
     ylab = 'central nucleotide methylation (beta value)')

plot(chh[['cg']], chh[['chh']],
     main = sprintf('CHH\n%s flanking cpgs and %s environments scrutinized\n pvalue %s, cor %s',
       FLANK,
       MAX,
       cor.test(chh[['cg']], chh[['chh']])$p.value,
       cor.test(chh[['cg']], chh[['chh']])$estimate),
     xlim = c(0,1),
     ylim = c(0,1),
     xlab = 'surrounding cgs methylation (beta value)',
     ylab = 'central nucleotide methylation (beta value)')
     

plot(control[['control']], control[['cg']],
     main = sprintf('control\n%s flanking cpgs and %s environments scrutinized\n pvalue %s, cor %s',
       FLANK,
       MAX,
       xlab = 'surrounding cgs',
       ylab = 'nucleotide',
       cor.test(control[['cg']], control[['control']])$p.value,
       cor.test(control[['cg']], control[['control']])$estimate),
     xlim = c(0,1),
     ylim = c(0,1),
     xlab = 'surrounding cgs methylation (beta value)',
     ylab = 'central nucleotide methylation (beta value)')     

dev.off()

