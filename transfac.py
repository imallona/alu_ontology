#!/usr/bin/env python
#
# Noble's precomputed TRANSFAC-based FIMO calculus on hg19

"""
@package alu_ontology
@author Izaskun Mallona
Gets the XML/RDF formalization of an OWL ontology of human Alus
"""

from utils import *
from unix_subprocesses import *
import os.path as op

TRANSFAC_BB = 'transfac.bb'
TRANSFAC = 'transfac.bed'

class Transfac:
    """
    A FIMO-based transcription factor binding sites getter
    """
    def get_big_bed(self, tmp_dir):
        """
        A method to fetch the Noble's precomputed data with transfac, fimo 
        on hg19
        """
        base_url = 'http://noble.gs.washington.edu/custom-tracks/'
        track = '/'.join([base_url, 'fimo.hg19.transfac-0.1.bb'])
        # track description
        descr = '/'.join([base_url, 'fimo.transfac.description.html'])

        unix = Unix_subprocesses()
        
        #unix.wget_getter(track, op.join(tmp_dir, TRANSFAC_BB))
        #unix.bigbed_to_bed(op.join(tmp_dir, TRANSFAC_BB), op.join(tmp_dir, TRANSFAC_BB))
        unix.wget_bed_getter(track, op.join(tmp_dir, TRANSFAC_BB), op.join(tmp_dir, TRANSFAC))

        return(op.join(tmp_dir, TRANSFAC))
#transfac = Transfac()
#unix = Unix_subprocesses()

#transfac.get_big_bed('/home/labs/maplab/imallona/tmp')        
#tfbs = transfac.summarize_intersecting_features()

        
