#!/bin/bash

LISTER_DATA="mc_h1.tar.gz"
TMP=~/tmp/lister
LISTER_FTP="ftp://ftpuser3:s3qu3nc3@neomorph.salk.edu/mc"
# liftover chain file
HG18_TO_HG19=hg18ToHg19.over.chain
HG18_TO_HG19_URL=http://hgdownload.cse.ucsc.edu/goldenPath/hg18/liftOver/"$HG18_TO_HG19".gz

# set -x

function usage() {
    echo "Awk and bedtools-based processing system for Lister's data"
    echo "  from doi:10.1038/nature08514"
    echo "Usage:" 
    echo "  bash methylation_utils.sh lister"
    exit 1
}

function get_basename() {
    bn=$(basename "$1")
    fn="${bn%.*}"
    echo "$fn"
}

# transforms the Lister's data to a BED-compliant format
function deprecated_lister_to_bed() {
    fn=$(get_basename "$1")
    awk '{OFS=FS="\t"; if (NR!=1) print "chr"$1, $2, $2+1, $4, "0", $3, $5, $6}' "$1" > "$fn".bed 
    echo "$fn".bed
}

# transforms the Lister's data to a BED-compliant format
# removing nonCG data
function lister_to_bed() {
    fn=$(get_basename "$1")
    awk '{OFS=FS="\t"; 
        if (NR != 1 && $4 == "CG") 
            print "chr"$1, $2, $2+1, $4, "0", $3, $5, $6}' "$1" > "$fn".bed 

    echo "$fn".bed    
}

function hg18_to_hg19() {
    fn=$(get_basename "$1")
    
    # beware, the columns above the third one are data (-bedPlus flag)
    liftOver -bedPlus=3 \
        "$fn".bed \
        "$TMP"/"$HG18_TO_HG19" \
        "$fn"_hg19.bed \
        "$fn"_unmapped.bed \

    echo "$fn"_hg19.bed
}

if [ $# = 0 ]
then
    usage
fi

# main

# # chunk that downloads lister data each run start

# # if the tempfolder exists, ask the user to erase it
# if [ -d "$TMP" ]
# then
#     echo The folder "$TMP", where the processing will be done at, already exists
#     read -p "Erase it?" -n 1 -r
#     echo
#     if [[ $REPLY =~ ^[Yy]$ ]]
#     then
# 	rm -rf $TMP
#     fi
# fi

# mkdir -p $TMP
# cd $TMP

# echo Getting the data
# wget -q "$LISTER_FTP"/"$LISTER_DATA"

# echo Extracting
# tar xzvf "$TMP"/"$LISTER_DATA"

# echo Getting the hg18 to hg19 liftOver chains

# wget  "$HG18_TO_HG19_URL" -O "$TMP"/"$HG18_TO_HG19".gz
# gunzip "$TMP"/"$HG18_TO_HG19".gz

# echo Converting to bedfiles
# mkdir -p $TMP/beds

# for file in $(ls "$TMP"/mc_h1/mc_h1*); 
# do
#     bed18=$(lister_to_bed "$file")
#     bed=$(hg18_to_hg19 "$bed18")
#     #echo $bed
#     mv "$bed" "$TMP"/beds
# done


# # chunk that downloads lister data each run end



# chunk that downloads lister once start

if [ -d "$TMP" ]
then
    # lister's data exist, do nothing
    echo Using previously processed lister data
    cd $TMP
else
    # lister's data download
    
    mkdir -p $TMP
    cd $TMP

    echo Getting the data
    wget -q "$LISTER_FTP"/"$LISTER_DATA"

    echo Extracting
    tar xzvf "$TMP"/"$LISTER_DATA"

    echo Getting the hg18 to hg19 liftOver chains

    wget  "$HG18_TO_HG19_URL" -O "$TMP"/"$HG18_TO_HG19".gz
    gunzip "$TMP"/"$HG18_TO_HG19".gz

    echo Converting to bedfiles
    mkdir -p $TMP/beds

    for file in $(ls "$TMP"/mc_h1/mc_h1*); 
    do
        bed18=$(lister_to_bed "$file")
        bed=$(hg18_to_hg19 "$bed18")
        #echo $bed
        mv "$bed" "$TMP"/beds
    done

    echo Getting the human Alus

    mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
        'SELECT genoName, genoStart,genoEnd,repName,repClass,strand,repFamily \
     FROM hg19.rmsk \
     WHERE repFamily = "Alu" \
      AND repName LIKE "Alu%"' | awk '{if (NR!=1) {print}}' > \
        "$TMP"/beds/hg19_alus_not_fram_nor_flam.bed

    echo Intersecting with the human Alus and grouping by mc and h values
    mkdir -p $TMP/intersected

    # cat "$TMP"/beds/mc*.bed | bedtools sort > "$TMP"/beds/lister_mc.bed
    #cat "$TMP"/beds/mc*.bed > "$TMP"/beds/lister_mc.bed
    #rm "$TMP"/beds/mc*.bed

    mkdir -p "$TMP"/grouped_by

    # serializing by chromosome 
    # todo: xargs this
    for file in $(ls "$TMP"/beds/mc*.bed);
    do
        fn=$(get_basename "$file")    
        
        # chr name
        chr=chr"$(echo $fn | cut -d'_' -f 3)"

        fgrep $chr "$TMP"/beds/hg19_alus_not_fram_nor_flam.bed | bedtools intersect -a stdin \
	    -b $file -loj > "$TMP"/intersected/"$fn"_alu_intersected.bed

        # mind that mC is the number of methylated bases called at a given position 
        # and h represents total number of sequenced bases at that position.
        # after the intersectBed, 
        #   mc is at column 14
        #   h is at column 15
        
        bedtools groupby -i "$TMP"/intersected/"$fn"_alu_intersected.bed \
	    -c 14 \
	    -o collapse | fgrep -v '.' > "$TMP"/grouped_by/"$fn"_mc.bed

        bedtools groupby -i "$TMP"/intersected/"$fn"_alu_intersected.bed \
	    -c 15 \
	    -o collapse | fgrep -v '.' > "$TMP"/grouped_by/"$fn"_h.bed

        # collapsed values are at column 5 on both bedtools groupby outputs
        # and are comma separated; processing will be done at the main python script
    done


fi


# chunk that downloads lister data once end

