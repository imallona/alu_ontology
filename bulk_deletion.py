#!/usr/bin/python
# -*- coding: utf-8  -*-
  
MATCHING_PATTERN = u'Alu*'

import wikipedia, pagegenerators, re, sys, catlib

gen = pagegenerators.SearchPageGenerator(MATCHING_PATTERN, 
                                         namespaces = "0")

preloadingGen = pagegenerators.PreloadingGenerator(gen, 
                                                   pageNumber=100)

while True:
    try:
        for page in preloadingGen:
            if page.isRedirectPage() or page.isDisambig():
                pass
            else:
                page.delete('Bulk deleting Alus with hg18 data')
    except StopIteration:
        gen = pagegenerators.SearchPageGenerator(MATCHING_PATTERN, 
                                                 namespaces = "0")
        preloadingGen = pagegenerators.PreloadingGenerator(gen, 
                                                       pageNumber=100)

