#!/usr/bin/ruby

latexhead = <<'EOF'
\documentclass{article}
\usepackage[pdftex]{graphicx}
\usepackage[margin=0.1in]{geometry}
\usepackage{pdfpages}
\begin{document}
EOF
latextail = <<'EOF'
\end{document}
EOF

pages = %x[pdfinfo #{ARGV[0]}].split(/\n/).select{|x| x=~ /Pages:/}[0].split(/\s+/)[1].to_i
puts latexhead
s = (1..pages).each_slice(9).to_a
s.each do |a|
  puts "\\begin{figure}"
  a.each do |p|
    puts "\\includegraphics[page=#{p},scale=0.33,width=.33\\textwidth]{#{ARGV[0]}}"
  end
  puts "\\end{figure}"
end
puts latextail
