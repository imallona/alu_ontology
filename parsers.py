#!/usr/bin/env python

from string import Template
from utils import *
from formatters import *
from rdfio_compliant_formatters import *
from unix_subprocesses import *

ENST = 0
ENSG = 1
CHROM = 2
STRAND = 3
CDS_START, CDS_END = 4, 5               # coding sequences start and end
TRANSCRIPT_START, TRANSCRIPT_END = 6, 7 # transcription start and end
GO = 9                                  # the go tag col index
DESCR = 10                              # the go description col index
GO_TYPE = 11                            # whether biological_project, cellular_location or molecular_function

class Parsers:
    """
    Parsers and iterators for UCSC partly processed data
    """

    def __init__(self, rdfio_compliant):
        """
        Whether the formatter that will be used is for RDFIO-compliant data
        population or just classic, Protege-compliant, OWL
        @param rdfio_compliant boolean whether RDFIO compliance is required
        """
        if rdfio_compliant: self.formatter = Rdfio_compliant_formatters()
        else: self.formatter = Formatters()
        
        self.unix = Unix_subprocesses()

    
    def parse_genes(self, indiv_fh, data, genes):
        """
        Generates individuals for ENSEMBL genes and transcripts
        @param indiv_fh a filehandle to write to
        @param data the data structure the iteration will be done at
        @param genes the genes dict
        """
        print ('[%s] Gene and transcript parsing start' %Utils().timestamp())
        prev_enst = data[0][ENST]
        bps, ccs, mfs = [], [], []
        
        for an in data:
            enst = an[ENST]
            if prev_enst == enst:
                if an[GO_TYPE] == 'biological_process': bps.append(an[GO])
                elif an[GO_TYPE] == 'cellular_component': ccs.append(an[GO])
                elif an[GO_TYPE] == 'molecular_function': mfs.append(an[GO])

                # thus finished to get the transcript's annotations
            else:
                indiv_fh.write(self.formatter.format_transcript(an[ENST], an[ENSG], 
                                                 list(set(bps)), list(set(ccs)), list(set(mfs))))
                bps, ccs, mfs = [], [], []

                # setting up the information for the associated gene and location
                loc = self.formatter.format_location(genes[an[ENSG]][0], 
                                                     genes[an[ENSG]][1],
                                                     genes[an[ENSG]][2],
                                                     genes[an[ENSG]][3])

                indiv_fh.write(loc[0])
                indiv_fh.write(self.formatter.format_gene(an[ENSG], loc[1]))        

            prev_enst = enst

        print ('[%s] Gene and transcript parsing end' %Utils().timestamp())

    
    def parse_merged_colors_and_genes(self, indiv_fh, merged, tfbs_dict, meth_dict):
        """
        Iterator over bedtools output that extracts the alu as well the closest gene and color
        @param indiv_fh the filehandle to write to
        @param merged the merged bedtools closestBed outputs
        @param tfbs_dict the dict written by unix.summarize_intersecting_features()
        @param meth_dict the dict written by Methylation().process_methylation()
        @merged the unix-based merged bedtools outputs
        """
        alu_chr, alu_start, alu_end, alu_strand, alu_family = 1, 2, 3, 6, 4
        ensg_chr, ensg_start, ensg_end, ensg_id, ensg_dist = 8, 9, 10, 11, 14
        #hmm_chrom, hmm_start, hmm_end, hmm_id, hmm_dist = 21, 22, 23, 24, 26
        hmm_chrom, hmm_start, hmm_end, hmm_dist = 22, 23, 24, 27
        hmm_id_t = Template('hmm.$chrom:$start-$end')

        # rather using the hmm tag as attribute
        hmm_color_attribute = 25
        
        for item in merged:
            alu  = item.split(' ')
            alu_id = '_'.join(map(str,alu[1:4]))          # tfbs fetching by dict
            if len(item) > 1:
                if alu_id in tfbs_dict.keys(): 
                    tfbs_properties = self.formatter.format_tfbs(alu_id, tfbs_dict)
                else: tfbs_properties = ''
                
                if alu_id in meth_dict.keys():
                    meth_properties = self.formatter.format_meth(alu_id, meth_dict)
                else: meth_properties = ''
                
                indiv_fh.write(self.formatter.format_alu(
                        chrom = alu[alu_chr], 
                        start = alu[alu_start],
                        end = alu[alu_end],
                        strand = alu[alu_strand],
                        distance = alu[ensg_dist], 
                        ensg = alu[ensg_id],
                        family = alu[alu_family],
                        length = str(int(alu[alu_end]) - int(alu[alu_start])),
                        # color = hmm_id_t.substitute(chrom = alu[hmm_chrom],
                        #                             start = alu[hmm_start],
                        #                             end = alu[hmm_end]),
                        color = alu[hmm_color_attribute],
                        tfbs = tfbs_properties,
                        meth = meth_properties))
    
    def stable_parse_merged_colors_and_genes(self, indiv_fh, merged, tfbs_dict):
        """
        Old backup, stable
        Iterator over bedtools output that extracts the alu as well the closest gene and color
        @param indiv_fh the filehandle to write to
        @param merged the merged bedtools closestBed outputs
        @param tfbs_dict, the dict written by unix.summarize_intersecting_features()
        @merged the unix-based merged bedtools outputs
        """
        alu_chr, alu_start, alu_end, alu_strand, alu_family = 1, 2, 3, 6, 4
        ensg_chr, ensg_start, ensg_end, ensg_id, ensg_dist = 8, 9, 10, 11, 14
        #hmm_chrom, hmm_start, hmm_end, hmm_id, hmm_dist = 21, 22, 23, 24, 26
        hmm_chrom, hmm_start, hmm_end, hmm_dist = 21, 22, 23, 26
        hmm_id_t = Template('hmm.$chrom:$start-$end')
        
        for item in merged:
            alu  = item.split(' ')
            alu_id = '_'.join(map(str,alu[1:4]))          # tfbs fetching by dict
            
            if len(item) > 1:
                if alu_id in tfbs_dict.keys(): 
                    tfbs_properties = self.formatter.format_tfbs(alu_id, tfbs_dict)
                else: tfbs_properties = ''
                
                indiv_fh.write(self.formatter.format_alu(
                        chrom = alu[alu_chr], 
                        start = alu[alu_start],
                        end = alu[alu_end],
                        strand = alu[alu_strand],
                        distance = alu[ensg_dist], 
                        ensg = alu[ensg_id],
                        family = alu[alu_family],
                        length = str(int(alu[alu_end]) - int(alu[alu_start])),
                        color = hmm_id_t.substitute(chrom = alu[alu_chr],
                                                    start = alu[alu_start],
                                                    end = alu[alu_end]),
                        tfbs = tfbs_properties))
    
    
    def parse_colors(self, indiv_fh, colors, cell_line):
        """
        Bundled with format_color for HMM colors
        @param indiv_fh the output fh
        @param colors the data estructure containing the closest colors
        @param cell_line string the line the chromatin color was defined at
        """
        
        for color in colors:
            chrom, start, end, name = color[0], color[1], color[2], color[3]
            
            indiv_fh.write(self.formatter.format_color(chrom = chrom, 
                                                       start = start, 
                                                       end = end, 
                                                       name = name,
                                                       strand = '+',
                                                       line = cell_line))
            

    def parse_tfbs(self, summarized):
        """
        @param summarized_fn the fn produced by unix.summarize_intersecting_features
        @return a dict with the tfbs and the Alu chrom, start, end joined by '_' as key
        """

        tfbs_dict = {}
        chrom, start, end, family, tfbs = 0, 1, 2, 3, 4
        
        for line in summarized.split('\n'):
            
            if len(line) > 0 : alu = line.split('\t')
            if alu[tfbs] != '.':                
                alu_id = '_'.join(map(str, alu[0:3]))
                tfbs_dict[alu_id] = alu[tfbs]
        
        return (tfbs_dict)
        
