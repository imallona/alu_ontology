#!/usr/bin/env python

import os
from os import path as op
import subprocess
import shutil
from string import Template
from utils import *

"""
@package alu_ontology
@author Izaskun Mallona
Gets the XML/RDF formalization of an OWL ontology of human Alus
"""

class Unix_subprocesses:
    """
    Utilities related with Unix built-ins, such merging, awking and the like
    In most of the cases, bedtools are used
    """
    def assign_closest_hmm(self, alus, hmm, tmp):
        """
        Closest chromatin color assign by bedtools
        @param alus object containing the alus (tuple of tuples)
        @param genes the dict containing the genes 
        @param tmp the temp folder that will be created and/or emptied afterwards
        """

        tmp = op.join(tmp, 'annotation')
        if not op.exists(tmp): os.makedirs(tmp)

        alus_fh = open(op.join(tmp, 'alus.bed'), 'w')
        hmm_fh = open(op.join(tmp, 'hmm.bed'), 'w')

        for alu in alus: 
            alus_fh.write('\t'.join(map(str, alu)) + '\n')

        for color in hmm:
            hmm_fh.write('\t'.join(map(str, color)) + '\n')


        alus_fh.close()
        hmm_fh.close()

        assigned = self.call_bedtools(op.join(tmp, 'alus.bed'),
                                      op.join(tmp, 'hmm.bed'),
                                      tmp)

        shutil.rmtree(tmp)

        return assigned

    def assign_closest_gene(self, alus, genes, tmp):
        """
        Closest gene assign by bedtools
        @param alus object containing the alus (tuple of tuples)
        @param genes the dict containing the genes 
        @param tmp the temp folder that will be created and/or emptied afterwards
        """

        tmp = op.join(tmp, 'annotation')
        if not op.exists(tmp): os.makedirs(tmp)

        alus_fh = open(op.join(tmp, 'alus.bed'), 'w')
        genes_fh = open(op.join(tmp, 'genes.bed'), 'w')

        for alu in alus: 
            alus_fh.write('\t'.join(map(str, alu)) + '\n')

        for gene in genes.keys():
            #genes_fh.write(gene + '\t' + '\t'.join(map(str, genes[gene])) + '\n')
            genes_fh.write(str(genes[gene][0]) + '\t' + \
                           str(genes[gene][1]) + '\t' + \
                           str(genes[gene][2]) + '\t' + \
                           gene + '\t' + \
                           '0\t' + \
                           str(genes[gene][3]) + '\n')

        alus_fh.close()
        genes_fh.close()

        assigned = self.call_bedtools(op.join(tmp, 'alus.bed'),
                                      op.join(tmp, 'genes.bed'),
                                      tmp)

        shutil.rmtree(tmp)

        return assigned

    def call_bedtools(self, a, b, tmp):
        """
        Calls a bedtools closestBed via a shell subprocess
        @param a the bedfile a
        @param b the bedfie b
        @param tmp the tmp folder the processing will be done at
        @return a list of bed-like strings
        """
        
        t = Template("closestBed -a $a -b $b -D a")
        cmd = t.substitute(a = a, b = b)

        print '[%s] %s start' %(Utils().timestamp(), cmd)

        proc = subprocess.Popen(cmd.split(), 
                                stdout = subprocess.PIPE, 
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) >0 : print err

        print '[%s] %s end' %(Utils().timestamp(), cmd)

        return out.split('\n')

    def merge_genes_and_chromatin_states(self, closest_gene, closest_color, temp):
        """
        Takes to lists of closestBed outputs and merges them according to their
        first three columns.
        Requires the two files to be sorted, as calls the unix 'join' as subprocess.
        @return the joined list (an item per alu)
        @param temp the temp folder the processing will be done at
        @param closest_gene the nearest gene
        @param closest_color the narest chromatin state
        """

        temp = op.join(temp, 'aaa')
        if not op.exists(temp): os.makedirs(temp)

        gen_fh = open(op.join(temp, 'genes'), 'w')
        color_fh = open(op.join(temp, 'colors'), 'w')

        for item in closest_gene:
            line = map(str, item.split('\t'))
            if len(line) > 3:
                gen_fh.write(line[0] + ':' + line[1] + '-' + line[2] + '\t' + item + '\n')

        for item in closest_color:
            line = map(str, item.split('\t'))
            if len(line) > 3:
                color_fh.write(line[0] + ':' + line[1] + '-' + line[2] + '\t' + item + '\n')

        gen_fh.close()
        color_fh.close()
        #cmd = 'join -1 1 -2 1 %s %s' %(op.join(temp, 'genes'), op.join(temp, 'colors'))
        cmd_t = Template('join -1 1 -2 1 $genes $colors')
        cmd = cmd_t.safe_substitute(genes = op.join(temp, 'genes'), 
                                    colors = op.join(temp, 'colors'))

        proc = subprocess.Popen(cmd.split(), 
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err

        shutil.rmtree(temp)
        
        return out.split('\n')
    
    def ontology_structure_builder(self, data_path, individuals_owl, out_owl):
        """
        Concatenates the already designed elements of the ontology to the individuals passed as parameter (that are computed by this very same script)
        @param individuals_owl the fn of the individuals file
        @param data_path, the folder which the other owl files are located at
        @param out_owl, the file that will receive the fully populated ontology
        """

        cmd_t = Template('cat $header $prop $obj $data $classes $pre_indiv $indiv $footer')        
        
        cmd = cmd_t.substitute(header = op.join(data_path, 'header.owl'),
                               prop =  op.join(data_path, 'annotation_properties.owl'),
                               obj = op.join(data_path, 'object_properties.owl'),
                               data = op.join(data_path, 'data_properties.owl'),
                               classes = op.join(data_path, 'classes.owl'),
                               pre_indiv = op.join(data_path, 'individuals.owl'),
                               indiv = individuals_owl,        
                               footer = op.join(data_path, 'footer.owl'))

        print '[%s] Materializing the ontology start' %(Utils().timestamp())

        proc = subprocess.Popen(cmd.split(),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err

        with open(out_owl, 'wb') as fh:
            fh.write(out)

        print '[%s] Materializing the ontology end' %(Utils().timestamp())
        
        return out_owl


    def rdfio_structure_builder(self, data_path, individuals_owl, out_owl):
        """
        Concatenates the already designed elements of the ontology to the individuals passed as parameter (that are computed by this very same script) in a manner compliant with the RDFIO tool for Semantic MediaWiki integration
        @param individuals_owl the fn of the individuals file
        @param data_path, the folder which the other owl files are located at
        @param out_owl, the file that will receive the fully populated ontology
        """

        cmd_t = Template('cat $header $indiv $footer')        
        
        print '[%s] Materializing the ontology start' %(Utils().timestamp())

        cmd = cmd_t.substitute(header = op.join(data_path, 'header_rdfio.owl'),
                               indiv = individuals_owl,        
                               footer = op.join(data_path, 'footer.owl'))

        proc = subprocess.Popen(cmd.split(),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err

        with open(out_owl, 'wb') as fh:
            fh.write(out)
        
        print '[%s] Materializing the ontology end' %(Utils().timestamp())

        return out_owl



    def wget_getter(self, url, destination):
        """
        wget tool
        @deprecated wget fetching tool
        """

        #cmd_t = Template('wget $url -P $dest')
        cmd_t = Template('wget $url -O $dest')
        cmd = cmd_t.substitute(url = url, dest = destination)
        
        proc = subprocess.Popen(cmd.split(),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err

        #with open(destination, 'wb') as fh:
        #    fh.write(out)

    def bigbed_to_bed(self, bb, bed):
        """
        @deprecated as the data retrieval and conversion may be done at once
        bigbedtobed converter tool
        """

        cmd = 'bigBedToBed %s %s' %(bb, bed)
        
        proc = subprocess.Popen(cmd.split(),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err
        
        with open(destination, 'wb') as fh:
            fh.write(out)
        
    def wget_bed_getter(self, url, dest_bb, dest_bed):
        """
        Retrieves silently a bigbedfile from a given url and transforms it
        using bigBedToBed into a bedfile
        Designed from FIMO-genome-wide-precomputed TRANSFAC motifs
        @param dest_bb, the destination bigbed
        @param dest_bed, the destination bedfile
        """

        wget_t = Template('wget $url -O $dest -q')
        bb_t = Template('bigBedToBed $infile $outfile')

        wget = wget_t.substitute(url = url, dest = dest_bb)
        bb = bb_t.substitute(infile = dest_bb, outfile = dest_bed)

        cmd = subprocess.Popen( ["-c", ';'.join([wget, bb]) ], shell = True )
        cmd.communicate()
        cmd.wait()
        
    def intersect_tfbs(self, bed_file, fimo_file):
        """
        A bedtools intersect tool for TFBS detection inside Alus
        @deprecated as summarize_intersecting_features(self, a, b) does handle this
        """

        cmd_t = 'bedtools intersect -a $bed_file -b $fimo_file -wb'
        cmd = cmd_t.substitute(bed_file = bed_file, fimo_file = fimo_file)
        
        proc = subprocess.Popen(cmd.split(),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err
        
        return(out)
        
    def summarize_intersecting_features(self, a, b):
        """
        intersectBed -a hg_19_track_repeatMasker_table_rmsk_repFamily_matches_Alu.bed -b transfac.bed -loj -wb | groupBy -g 1,2,3,4 -c 10 -o collapse > colapsed
        
        @param a bed file, i.e. the hg19 aluome
        @param b the transfac bed obtained from wget_bed_getter
        @return the summarized bedfile
        """

        # intersects the alu file (a) to the transfac file (b) and collapses the output
        # using the first four fields of the Alu bedfile (chr, start, end, name)
        # and summarizing as output the name of the tfbs (column 10 of the b param)
        cmd_t = Template('intersectBed -a $a -b $b -loj -wb | groupBy -g 1,2,3,4 -c 11 -o collapse')

        cmd = cmd_t.substitute(a = a, b = b)

        proc = subprocess.Popen( ["-c", cmd ], 
                                stdout = subprocess.PIPE, 
                                stderr = subprocess.PIPE,
                                shell = True)
        (out, err) = proc.communicate()
        proc.wait()

        if len(err) > 1: print(err)
        return(out)
        
    def tgz_extract(self, tgzfn):
        """
        Extracts a tgz file using tar
        @param tgzfn the tgz filename
        """
        
        cmd_t = Template('tar xzvf $tgz')
        cmd = cmd_t.substitute(tgz = tgzfn)
        
        proc = subprocess.Popen(cmd.split(),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE)
        (out, err) = proc.communicate()

        if len(err) > 0: print err

    def call_bash_script(self, script):
        """
        Calls a subprocess interpreting some script
        @param script the bash script
        """
        print 'Calling %s as subprocess' %script 
        
        proc = subprocess.Popen('bash %s' %script,
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE,
                                shell = True)
        (out, err) = proc.communicate()

        if len(err) > 0: print err
        return (out, err)
        
