#!/usr/bin/env R
##
## Mon Mar  27 2015

library(vioplot)

HOME <- '/home/labs/maplab/imallona'
WDIN <- file.path(HOME, 'pfc','bedtools_gene_centered_by_chr')
NPERM <- 1000


## functions start

plot_mean_vs_sd <- function(by_location,  location, promoter) {
    plot(by_location[[location]][['mean']][[promoter]],
         by_location[[location]][['stdev']][[promoter]],
         col = rgb(0,0,0,0.2), pch = 16,
         main = sprintf('%s\nN=%s\n%s',
             promoter,
             length(by_location[[location]][['stdev']][[promoter]]),
             location),
         xlab = 'mean beta value',
         ylab = 'sd beta value',
         xlim = c(0,1))    
}

## functions end

by_chromosome <- list()
## serializing over chromosomes
for (chr in 1:22) {
    


    METHWD <- file.path(WDIN, chr, 'locations')

    SUMMARIES <- c('mean', 'stdev')

    promoters <- c('1_Active_Promoter','2_Weak_Promoter','3_Poised_Promoter')

    dat <- list()

    for (summary in SUMMARIES) {
        dat[[summary]] <- list()
        for (promoter in promoters) {
            ## print(sprintf('mc*%s', promoter))
            for (fn in list.files(file.path(METHWD, summary), pattern = sprintf('*%s', promoter))) {
                ## print(file.path(METHWD, fn))
                ## foo <- tryCatch(read.table(file.path(METHWD, fn), sep = "\t"),
                ##                 error = print(file.path(METHWD, fn)))

                tryCatch({
                    foo <- read.table(file.path(METHWD, summary, fn), sep = "\t")

                    
                    this <- foo$V14
                    dat[[summary]][[promoter]] <- c(dat[[summary]][[promoter]], this)
                    
                }, error = function(x) return(fn))
            }
        }        
    }
    



    ## upstream, downstream and inside features

    locations <- c('upstream', 'downstream', 'inside')

    by_location <- list()
    for (location in locations) {   
        filtered_dat <- list()

        for (summary in SUMMARIES) {
            filtered_dat[[summary]] <- list()
            for (promoter in promoters) {
                ## print(sprintf('mc*%s', promoter))
                for (fn in list.files(file.path(METHWD, summary),
                                      pattern = sprintf('*%s.*%s', location, promoter))) {
                    ## print(file.path(METHWD, fn))
                    ## foo <- tryCatch(read.table(file.path(METHWD, fn), sep = "\t"),
                    ##                 error = print(file.path(METHWD, fn)))

                    tryCatch({
                        foo <- read.table(file.path(METHWD, summary, fn), sep = "\t", stringsAsFactors = FALSE)

                        
                        this <- foo$V14
                        filtered_dat[[summary]][[promoter]] <- c(filtered_dat[[summary]][[promoter]], this)
                        
                    }, error = function(x) return(fn))
                }
            }        
        }

        by_location[[location]] <- filtered_dat
    }

    pdf(file.path(METHWD, 'mean_vs_sd_panel.pdf'), useDingbats = FALSE)
    ## png(file.path(METHWD, 'mean_vs_sd_panel.png'), height = 600, width = 600)




    par(mfrow=c(3,3))

    for (promoter in promoters) {
        for (location in locations) {
            ## plot_mean_vs_sd(by_location, 'upstream', '1_Active_Promoter')
            plot_mean_vs_sd(by_location, location, promoter)
        }
    }


    dev.off()

    for (i in 1:3) {
        plot(filtered_dat[['mean']][[i]], filtered_dat[['stdev']][[i]],
             col = rgb(0,0,0,0.2), pch = 16,
             main = sprintf('%s\nN=%s\n%s',
                 names(filtered_dat[['stdev']])[[i]],
                 length(filtered_dat[['stdev']][[i]]),
                 location),
             xlab = 'mean beta value',
             ylab = 'sd beta value',
             xlim = c(0,1))
    }
    ## dev.off()

    pdf(file.path(METHWD, 'violinplots.pdf'), useDingbats = FALSE)

    ## op <- par(mar = c(4,4,4,2) + 0.1) ## default is c(5,4,4,2) + 0.1

    vioplot(by_location[['upstream']][['mean']][['1_Active_Promoter']],
            by_location[['upstream']][['mean']][['2_Weak_Promoter']],
            by_location[['upstream']][['mean']][['3_Poised_Promoter']],

            by_location[['inside']][['mean']][['1_Active_Promoter']],
            by_location[['inside']][['mean']][['2_Weak_Promoter']],
            by_location[['inside']][['mean']][['3_Poised_Promoter']],

            by_location[['downstream']][['mean']][['1_Active_Promoter']],
            by_location[['downstream']][['mean']][['2_Weak_Promoter']],
            by_location[['downstream']][['mean']][['3_Poised_Promoter']],

            names = c('U\nA','U\nW','U\nP',
                'I\nA','I\nW','I\nP',
                'D\nA','D\nW','D\nP'),

            col = 'gray60',
            ylim = c(0,1))


    vioplot(by_location[['upstream']][['mean']][['1_Active_Promoter']],
            by_location[['inside']][['mean']][['1_Active_Promoter']],
            by_location[['downstream']][['mean']][['1_Active_Promoter']],
            
            by_location[['upstream']][['mean']][['2_Weak_Promoter']],       
            by_location[['inside']][['mean']][['2_Weak_Promoter']],
            by_location[['downstream']][['mean']][['2_Weak_Promoter']],

            by_location[['upstream']][['mean']][['3_Poised_Promoter']],
            by_location[['inside']][['mean']][['3_Poised_Promoter']],
            by_location[['downstream']][['mean']][['3_Poised_Promoter']],

            names = c('U\nA','I\nA','D\nA',
                'U\nW','I\nW','D\nW',
                'U\nP','I\nP','D\nP'),

            col = 'gray60',
            ylim = c(0,1))

    dev.off()

    by_chromosome[[as.character(chr)]] <- by_location

}

