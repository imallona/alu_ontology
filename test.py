

import sys
import os
import shutil
import re
from parsers import *
from ucsc_mysql_shuttle import *
from unix_subprocesses import *
from utils import *
from transfac import * 
from methylation import *

TMP= '/home/labs/maplab/imallona/tmp'
INDIVIDUALS_FN = op.join(TMP, 'individuals2.owl')
GO_TERMS_FN = op.join(TMP, 'go.owl')
DATA = '/imppc/labs/maplab/imallona/src/ontology/data'

rdfio_compliant = False

"""
Main method of ontology making
@param rdfio_compliant boolean, whether the output is desired to be piped to RDFIO /SMW 
or to Protege
"""

shuttle = Ucsc_mysql_shuttle()
db = shuttle.db_connect()

data = shuttle.ucsc_data_getter(db)
genes = shuttle.gene_location_getter(db)
alus = shuttle.alu_getter(db)
colors = shuttle.hmm_hesc_color_getter(db) 

if op.exists(INDIVIDUALS_FN): os.remove(INDIVIDUALS_FN)
if op.exists(GO_TERMS_FN): os.remove(GO_TERMS_FN)

indiv_fh = open(INDIVIDUALS_FN, 'a')
go_terms_fh = open(GO_TERMS_FN, 'a')

# iteration based in retrieving all the annotations for a given transcript
# 
# the individuals owl file is generated
# as well the go should; but some go slimming must be introduced here @todo
parsers = Parsers(rdfio_compliant)

parsers.parse_genes(indiv_fh, data, genes)

unix = Unix_subprocesses()
closest_gene = unix.assign_closest_gene(alus, genes, TMP)
closest_color = unix.assign_closest_hmm(alus, colors, TMP)

merged = unix.merge_genes_and_chromatin_states(closest_gene, closest_color, TMP)

# tfbs start
transfac = Transfac()
tfbs_fn = transfac.get_big_bed(TMP)
alus_fn = op.join(TMP, 'alus.bed')
with open(alus_fn, 'w') as alus_fh:
    for alu in alus:
        alus_fh.write('\t'.join(map(str, alu)) + '\n')


tfbs_intersected = unix.summarize_intersecting_features(alus_fn, tfbs_fn)
        
tfbs_dict = parsers.parse_tfbs(tfbs_intersected) 

print 'meth test start'


# meth_dict = Methylation().process_methylation()
m = Methylation()

(out, err) = m.fetch_lister_data()
print (out)
        
mc_files = glob.glob(op.join(m.GROUPED_BY_FOLDER, '*mc*'))
h_files = glob.glob(op.join(m.GROUPED_BY_FOLDER, '*h*'))

print(len(mc_files))

# the dict is updated for each chromosome sequentially
meth_values = {}
i = 0
for i in range(len(mc_files)):
    meth_values = m.get_meth_values(meth_values, mc_files[i], h_files[i])




# print(len(meth.keys()))


print 'meth test end'
meth_dict = meth_values

parsers.parse_merged_colors_and_genes(indiv_fh, merged, tfbs_dict, meth_dict)

# tfbs end

parsers.parse_colors(indiv_fh, colors, 'hESC')

db.close()
indiv_fh.close()
go_terms_fh.close()

if rdfio_compliant: 
    unix.rdfio_structure_builder(DATA, INDIVIDUALS_FN, op.join(TMP, 'for_rdfio.owl'))
else:
    unix.ontology_structure_builder(DATA, INDIVIDUALS_FN, op.join(TMP, 'merged.owl'))
    
print 'done'
        
