#!/bin/bash
#
# 7 May 2015

set -e

NUM_THREADS=1
BASE=$HOME/pfc/bedtools_shuffled
# NPERM=1000
NPERM=100
LISTER_SCRIPT="$SRC"/ontology/methylation_utils.sh
#the previous produces its outputs at
LISTER_BEDS=~/tmp/lister/beds/

HMMS=(10_Txn_Elongation 11_Weak_Txn 12_Repressed 13_Heterochrom/lo 14_Repetitive/CNV 15_Repetitive/CNV 1_Active_Promoter 2_Weak_Promoter 3_Poised_Promoter 4_Strong_Enhancer 5_Strong_Enhancer 6_Weak_Enhancer 7_Weak_Enhancer 8_Insulator 9_Txn_Transition)

PROMOTERS=(1_Active_Promoter 2_Weak_Promoter 3_Poised_Promoter)

bedtools=/soft/bio/bedtools2-2.23.0/bin/bedtools

mkdir -p "$BASE"

cd "$BASE"

echo 'Retrieving the Alu coordinates'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT genoName, genoStart,genoEnd,repName,repClass,strand,repFamily \
     FROM hg19.rmsk \
     WHERE repFamily = "Alu" \
      AND repName LIKE "Alu%"' | awk '{if (NR!=1) {print}}' > "$BASE"/alus.bed


echo 'Retrieving chromatin colors'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT chrom, chromStart, chromEnd, name 
    FROM hg19.wgEncodeBroadHmmH1hescHMM' | awk '{if (NR!=1) {print}}' > "$BASE"/colors.bed

echo 'Retrieving methylation data from Lister'

# bash "$LISTER_SCRIPT" lister

echo 'Retrieving genes'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT chrom, min(txStart), max(txEnd), strand, name2
    FROM hg19.ensGene  
    GROUP BY name2' |  awk '{if (NR!=1) {print}}' > "$BASE"/genes.bed

echo Sorting

sort -k1,1 -k2,2n alus.bed > foo; mv foo alus.bed
sort -k1,1 -k2,2n colors.bed > foo; mv foo colors.bed
sort -k1,1 -k2,2n genes.bed > foo; mv foo genes.bed


echo 'Shuffled background to test for enrichments'
echo 'Being testing right now!'

IN=$BASE/in
MID=$BASE/mid
OUT=$BASE/out

mkdir -p $IN $MID $OUT

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e  "SELECT chrom, size FROM hg19.chromInfo" \
    > $BASE/hg19.genome


for i in $(seq 1 $NPERM)
do
    $bedtools shuffle -i $BASE/alus.bed -excl $BASE/alus.bed \
        -g $BASE/hg19.genome -noOverlapping -seed $i | sortBed > $IN/shuffled_"$i".bed

    
    $bedtools closest -a $IN/shuffled_"$i".bed -b $BASE/genes.bed -D "b"  > $IN/shuffled_"$i"_genes.intersect

    ## downstream means the alu is downstream of the gene
    awk '$13 > 0 && $13 < 1001'  $IN/shuffled_"$i"_genes.intersect > $IN/shuffled_"$i"_genes_downstream.bed
    awk '$13 < 0 && $13 > -1001' $IN/shuffled_"$i"_genes.intersect > $IN/shuffled_"$i"_genes_upstream.bed
    awk '$13 == 0' $IN/shuffled_"$i"_genes.intersect > $IN/shuffled_"$i"_genes_inside.bed

    rm  $IN/shuffled_"$i".bed $IN/shuffled_"$i"_genes.intersect

done

for i in $(find $IN -maxdepth 1 -iname "shuffled*genes*.bed" -printf '%f\n'  ) 
do

    $bedtools intersect -a $IN/$i  \
        -b $BASE/colors.bed -wo -sorted > $MID/"$i".color

    # coloring
    for promoter in ${PROMOTERS[@]} 
    do
        fgrep $promoter $MID/"$i".color > $MID/"$i".$promoter
      
    done

    rm $IN/$i $MID/"$i".color

done


echo 'assigning the DNA meth'

for meth_chrom in $(ls "$LISTER_BEDS"/mc*bed)
do
    for promoter in ${PROMOTERS[@]} 
    do
        # this contains the colors
        for fn in $(ls $MID/shuffled*genes*"$promoter")
        do
            fn=$(basename "$fn")
            meth_chrom=$(basename "$meth_chrom")
            $bedtools intersect -a  $MID/$fn \
                -b $LISTER_BEDS/$meth_chrom -wa -wb  > $MID/"$meth_chrom"_"$fn"
           

        done
    done
done



echo 'betas'

mkdir -p $MID/betas

for fn in $(find  $MID -maxdepth 1 -iname "mc*shuffled*Promoter" -printf '%f\n')
do
    awk '{OFS=FS="\t"; print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$25/$26}' $MID/$fn > \
        $MID/betas/$fn
    rm $MID/$fn
done


echo 'getting either mean, either sd for each Alu'

summaries=(mean stdev)

for summary in ${summaries[@]}
do
    mkdir -p $OUT/$summary
    
    for fn in $(find $MID/betas -iname "mc*shuffled*Promoter" -printf '%f\n' )
    do

        if [ -s $MID/betas/"$fn" ]
            then

            $bedtools groupby -i $MID/betas/$fn -g 1,2,3,4,5,6,7,8,9,10,11,12,13 \
                -c 14 -o $summary  > $OUT/$summary/$fn
        fi
        # rm $fn
    done
done

rm -rf $IN $MID

echo 'Done'


echo 'Further classification (check the source code)'
# Further classification

# for fn in $(seq 1 100)
# do 
#     for found in $(find mean -iname "mc*shuffled_"$fn"_*")
#     do 
#         mv $found  shuffled_"$fn"/mean
#     done
# done


# for fn in $(seq 1 100)
# do 
#     for found in $(find stdev -iname "mc*shuffled_"$fn"_*")
#     do 
#         mv $found  shuffled_"$fn"/stdev
#     done
# done

echo 'Further R plotting (check the source code)'
# Rscript bedtools_shuffling.R

echo 'Furterh pdf-based montage (check the source code), ruby'


# #!/usr/bin/ruby

# latexhead = <<'EOF'
# \documentclass{article}
# \usepackage[pdftex]{graphicx}
# \usepackage[margin=0.1in]{geometry}
# \usepackage{pdfpages}
# \begin{document}
# EOF
# latextail = <<'EOF'
# \end{document}
# EOF

# pages = %x[pdfinfo #{ARGV[0]}].split(/\n/).select{|x| x=~ /Pages:/}[0].split(/\s+/)[1].to_i
# puts latexhead
# s = (1..pages).each_slice(9).to_a
# s.each do |a|
#   puts "\\begin{figure}"
#   a.each do |p|
#     puts "\\includegraphics[page=#{p},scale=0.33,width=.33\\textwidth]{#{ARGV[0]}}"
#   end
#   puts "\\end{figure}"
# end
# puts latextail


####

# cd ~/pfc/bedtools_organized_shuffled/out
# for rseed in $(seq 1 100)
# do 
#     cd "shuffled_""$rseed"
#     ruby $SRC/ontology/pdf_montage_3_by_3.rb scatter_plots_with_marginals.pdf > montage_"$rseed".tex
#     pdflatex montage_"$rseed".tex   
#     cd ..
# done

# pdftk montage_*pdf cat output montage_scatter_marginals.pdf


### adding header

# \documentclass[8pt]{article}
# \usepackage[final]{pdfpages}
# \usepackage{fancyhdr}

# \topmargin 10pt
# \oddsidemargin 70pt

# \pagestyle{fancy}
# \fancyhead[C]{shuffled Alu elements with random seed \thepage}
# % \rfoot{random\_seed \Large\thepage}
# \cfoot{}
# \renewcommand {\headrulewidth}{0pt}
# \renewcommand {\footrulewidth}{0pt}

# \begin{document}
# \includepdfset{pagecommand=\thispagestyle{fancy}}
# \includepdf[fitpaper=true,scale=0.98,pages=-]{montage_scatter_marginals.pdf}
# % fitpaper & scale aren't always necessary - depends on the paper being submitted.
# \end{document}
