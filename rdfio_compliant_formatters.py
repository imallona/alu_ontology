#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@package alu_ontology
@author Izaskun Mallona
Gets the XML/RDF formalization of an OWL ontology of human Alus
"""

from string import Template

class Rdfio_compliant_formatters:    
    """
    The RDFIO/SMW complaint ontology formatter
    """
    def format_gene_ontology(self, go_id, go_descr):
        """
        @todo Mind that the go must be slimmed to the go slim prsent at the ontology level
        using map2slim.pl
        available to at http://amigo.geneontology.org/cgi-bin/amigo/slimmer
        @param go_id string, the gene ontology id
        @param go_descr string, the gene ontology description
        @return the substituted template
        """
        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#$go_descr -->

        <AluWiki:GO rdf:about="$go_descr">
            <go_id>$go_id</go_id>
            <go_descr>$go_descr</go_descr>
        </AluWiki:GO>
        """)

        return t.substitute(go_id = go_id, go_descr = go_descr)


    def format_gene(self, ensg, loc):
        """
        The gene formatter.
        
        Mind that this gets the cdsStart and cdsEnd of the ensGene table and thus the information
        passed to the different transcript variants does not contain which exons are mantained;
        a reimplementation of the transcript individuals including txStart and txEnd may make
        sense @todo
        @param ensg string, the ensemble gene id
        @param loc string, the location
        @return the substituted template
        """
        
        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#$ensg -->

        <SO:0000704 rdf:about="$ensg">
            <located_in>$loc</located_in>
        </SO:0000704>
        """)

        return t.substitute(ensg = ensg, loc = loc)


    def format_location(self, chrom, start, end, strand):
        """
        Formats a genomic coordinate that will be associated to a given feature, such as 
        a transcript, gene, mark or the like
        @param strand must be either 'positive' or 'negative'
        @return a tuple containing the loc accession as well the owl substituted template
        """
        
        if strand == '+': strand = 'positive'
        elif strand == '-': strand = 'negative'
        if not str(chrom).startswith('chr'): chrom = 'chr' + str(chrom)

        # the accession or identifier that will be used to assign the location to a feature
        acc_t = Template('Loc.$chrom:$start-$end;$strand')

        # the owl code to represent the individual
        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#Loc.$chrom:$start-$end;$strand -->

        <SO:0001411 rdf:about="Loc.$chrom.$start-$end;$strand">
            <on_chromosome>$chrom</on_chromosome>
            <has_start_point>$start</has_start_point>
            <has_end_point>$end</has_end_point>
            <on_strand>$strand</on_strand>
        </SO:0001411>
        """)

        return (t.safe_substitute(chrom = chrom, start = start, end = end, strand = strand),
                acc_t.safe_substitute(chrom = chrom, start = start, end = end, strand = strand))


    def format_transcript(self, enst, ensg, bp, cc, mf):
        """
        Formats a transcript and its annotations (GO)
        Takes as many bp, cc and mf annotations as provided (lists)
        @param enst string the enst
        @param ensg string the ensg
        @param bp list of strings of biological processes
        @param cc list of strings of cellular locations
        @param mf list of strings of molecular functions
        @return the substituted template
        """

        t = ''
        header_t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#$enst -->

        <SO:0000833 rdf:about="$enst">
            <transcribed_from>$ensg</transcribed_from>
        """)

        footer = """
        </SO:0000833>
        """

        t += header_t.substitute(enst = enst, ensg = ensg)

        if len(bp) > 0:
            bp_t = Template('<biological_process_player>$bp</biological_process_player>\n')
            for item in bp: t += bp_t.substitute(bp = item)

        if len(cc) > 0:
            cc_t = Template('<located_in_cellular_component>$cc</located_in_cellular_component>\n')        
            for item in cc: t += cc_t.substitute(cc = item)

        if len(mf) > 0:
            mf_t = Template('<involved_in_molecular_function>$mf</involved_in_molecular_function>\n')
            for item in mf: t += mf_t.substitute(mf = item)

        t += footer

        return(t)


    def format_alu(self, chrom, start, end, strand, distance, ensg, family, length, color):
        """
        Formats a given Alu
        @param chrom string the chromosome
        @param start string the start
        @param end string the end
        @param strand string the strand
        @param distance int the distance to a given gene
        @param ensg string the ensembl gene id
        @param family string the alu family
        @param length int the alu length
        @param color string the HMM chromatin state color
        @return the substituted Alu template
        @todo change the length computation to be done with the location
        """
 

        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#alu.$chrom:$start-$end;$strand -->

        <AluWiki:Alu rdf:about="alu.$chrom.$start-$end;$strand">
            <has_distance_to_nearest_gene>$distance</has_distance_to_nearest_gene>
            <has_length>$length</has_length>
            <has_Alu_family>$family</has_Alu_family>
            <closest_gene_to_Alu>$ensg</closest_gene_to_Alu>
            <located_in>Loc.$chrom:$start-$end;$strand</located_in>
            <closest_chromatin_state_to_Alu>$color</closest_chromatin_state_to_Alu>
        </AluWiki:Alu>
        """)

        return t.substitute(chrom = chrom, start = start, end = end, 
                            strand = strand, distance = distance, ensg = ensg,
                            family = family, length = length, color = color)



    def format_color(self, chrom, start, end, name, strand, line):
        """
        Formats the hmm_hesc data retrieved with the hmm_hesc_color_getter() function
        Mind that the HMM do not have 'strand', although is requested
        @param chrom string the chromosome
        @param start string the start
        @param end string the end
        @param strand string the strand
        @param name string the HMM chromatin state color
        @param line string the cell line the HMM was defined at
        @return the substituted chromatin state template
        """
        t = Template("""
        <!-- http://gattaca.imppc.org/groups/maplab/imallona/ontologies/alu_ontology.owl#hmm.$chrom:$start-$end -->

        <AluWiki:HMM rdf:about="hmm.$chrom.$start-$end">
            <hmm>$name</hmm>
            <located_in>Loc.$chrom.$start-$end;$strand</located_in>
            <has_cell_line>$line</has_cell_line>
        </AluWiki:HMM>
        """)

        return t.substitute(chrom = chrom, start = start, 
                            end = end, name = name, strand = strand, 
                            line = line)
