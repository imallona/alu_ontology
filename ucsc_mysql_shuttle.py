#!/usr/bin/env python

import MySQLdb
from utils import *

class Ucsc_mysql_shuttle:
    """
    Connects to the UCSC MySQL public server to fetch the data
    for the Alu ontology population
    """
    def db_connect(self):
        """
        @return a connection to the database
        """
        db = MySQLdb.connect(host = 'genome-mysql.cse.ucsc.edu',
                             user = 'genome')
        return (db)

    def ucsc_data_getter(self, db, chrom):
        """
        Fetches the genomic data from the UCSC public MySQL Server
        @param db the database connection
        """
        
        print ('[%s] Bulk UCSC MySQL query start' %Utils().timestamp())

        db = MySQLdb.connect(host = 'genome-mysql.cse.ucsc.edu',
                             user = 'genome')
        cur = db.cursor() 

        query = """
    SELECT eg.name as enst, eg.name2 as ensg, eg.chrom, eg.strand, eg.txStart, eg.txEnd, eg.cdsStart, eg.cdsEnd, 
        xref.geneSymbol, 
        go.goId, got.name, got.term_type 
    FROM hg19.ensGene as eg, hg19.kgXref as xref, go.goaPart as go, hg19.knownToEnsembl as kte, go.term as got 
    WHERE kte.value = eg.name AND 
        kte.name = xref.kgID AND 
        xref.spId = go.dbObjectId AND 
        go.goID = got.acc AND
        eg.chrom = '%s';
    """ %chrom

        # cur.execute(tiny_query)
        cur.execute(query)

        print ('[%s] Bulk UCSC MySQL query end' %Utils().timestamp())    
        return (cur.fetchall())

    def alu_getter(self, db, chrom):
        """
        Fetches from the UCSC public MySQL Server some data from hg19 Alus
        @param db, the database connection
        """

        print ('[%s] UCSC MySQL query regarding Alus start' %Utils().timestamp())

        cur = db.cursor()

        query = """
    SELECT genoName, genoStart,genoEnd,repName,repClass,strand,repFamily
    FROM hg19.rmsk 
    WHERE repFamily = "Alu" 
    AND repName LIKE "Alu%s" AND
    genoName = '%s';
    """ %('%', chrom)
        # cur.execute(tiny_query)
        cur.execute(query)


        print ('[%s] UCSC MySQL query regarding Alus end' %Utils().timestamp())
        return (cur.fetchall())


    def gene_location_getter(self, db):    
        """
        Fetches the lowest txStart and longest txEnd of the transcripts of a given gene to assign
        a genomic coordinate to it 
        Queries the UCSC public MySQL server
        @param db the database connection
        @return a dictionary of locations hashed by ensg identifier
        """

        print ('[%s] UCSC MySQL query regarding gene locations start' %Utils().timestamp())
        cur = db.cursor()

        query = """
    SELECT name2, chrom, min(txStart), max(txEnd), strand 
    FROM hg19.ensGene  
    GROUP BY name2
    """
        cur.execute(query)

        print ('[%s] UCSC MySQL query regarding gene locations end' %Utils().timestamp()) 

        genes = cur.fetchall()
        genesd = {a:[b,c,d,e] for a,b,c,d,e in genes}

        return (genesd)

    def hmm_hesc_color_getter(self, db, chrom):

        """
        Queries the UCSC data for 
        Schema for Broad ChromHMM - Chromatin State Segmentation by HMM from ENCODE/Broad 
        @param db the database connection
        """    
        
        print ('[%s] UCSC MySQL query regarding HMM for hESC start' %Utils().timestamp())

        query = """
    SELECT chrom, chromStart, chromEnd, name 
    FROM hg19.wgEncodeBroadHmmH1hescHMM
    WHERE chrom = '%s';
        """ %chrom

        cur = db.cursor()

        # cur.execute(tiny_query)
        cur.execute(query)

        print ('[%s] UCSC MySQL query regarding HMM for hESC end' %Utils().timestamp())

        return (cur.fetchall())

def methylation_getter(self, db, chrom):
    """
    @deprecated
    Queries the UCSC for RRBS meth data form hESC, replicate 1
    @param db the database connection

    Filters the data to a minium of 10 reads
    @return a fetchall bed6-formatted
    """
    
    query = """
SELECT chrom, chromStart, chromEnd, name, percentMeth, strand  
FROM hg19.wgEncodeHaibMethylRrbsH1hescHaibSitesRep1 
WHERE score >= 10 AND
    chrom = '%s';
    """ %chrom

    cur = db.cursor()
    
    # cur.execute(tiny_query)
    cur.execute(query)
    
    return (cur.fetchall())
