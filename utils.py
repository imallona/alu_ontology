#!/usr/bin/env python

"""
@package alu_ontology
@author Izaskun Mallona
Gets the XML/RDF formalization of an OWL ontology of human Alus
"""


import datetime

class Utils:
    """
    Simple utilities
    """
    def timestamp(self):
        """
        Returns a formatted timestamp
        """
        return(datetime.datetime.now().strftime('%d %b %Y %H:%M'))
